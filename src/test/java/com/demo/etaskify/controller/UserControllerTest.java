package com.demo.etaskify.controller;

import com.demo.etaskify.dto.UserRequestDto;
import com.demo.etaskify.dto.UserResponseDto;
import com.demo.etaskify.model.User;
import com.demo.etaskify.service.LoggedUserService;
import com.demo.etaskify.service.UserService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.when;
import static org.mockito.Mockito.verify;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
class UserControllerTest {

    private static final Long DUMMY_ID = 1L;
    private static final String DUMMY_STRING = "string";
    private static final String DUMMY_EMAIL = "dummy@dummy.com";
    private static final String BASE_URL = "/api/v1/users";
    private User user;

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

    @MockBean
    private LoggedUserService loggedUserService;

    @MockBean
    private UserService userService;

    private UserRequestDto userRequestDto;
    private UserResponseDto userResponseDto;

    @BeforeEach
    void setUp() {
        user = User.builder()
                .email(DUMMY_STRING)
                .build();

        userResponseDto = UserResponseDto
                .builder()
                .id(DUMMY_ID)
                .name(DUMMY_STRING)
                .email(DUMMY_EMAIL)
                .surname(DUMMY_STRING)
                .build();

        userRequestDto = UserRequestDto
                .builder()
                .name(DUMMY_STRING)
                .email(DUMMY_EMAIL)
                .surname(DUMMY_STRING)
                .build();
    }

    @Test
    @WithMockUser(roles = "ADMIN")
    void givenValidInputWhenCreateThenReturnOk() throws Exception {
        //Arrange
        when(userService.create(userRequestDto)).thenReturn(userResponseDto);
        when(loggedUserService.getLoggedUser()).thenReturn(user);

        //Act
        ResultActions resultActions = mockMvc.perform(post(BASE_URL)
                .content(objectMapper.writeValueAsString(userRequestDto))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON));

        //Assert
        resultActions.andExpect(status().isCreated());
        verify(userService, times(1)).create(userRequestDto);
    }
}
