package com.demo.etaskify.controller;

import com.demo.etaskify.dto.TaskRequestDto;
import com.demo.etaskify.dto.TaskResponseDto;
import com.demo.etaskify.model.User;
import com.demo.etaskify.service.LoggedUserService;
import com.demo.etaskify.service.TaskService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;

import java.time.LocalDate;
import java.util.List;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.when;
import static org.mockito.Mockito.verify;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
class TaskControllerTest {

    private static final Long DUMMY_ID = 1L;
    private static final String DUMMY_STRING = "string";
    private static final LocalDate DUMMY_DATE = LocalDate.parse("2021-12-12");
    private static final String BASE_URL = "/api/v1/tasks";
    private User user;

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

    @MockBean
    private TaskService taskService;

    @MockBean
    private LoggedUserService userService;

    private TaskRequestDto taskRequestDto;
    private TaskResponseDto taskResponseDto;

    @BeforeEach
    void setUp() {
        user = User.builder()
                .email(DUMMY_STRING)
                .build();

        taskResponseDto = TaskResponseDto
                .builder()
                .id(DUMMY_ID)
                .deadline(DUMMY_DATE)
                .description(DUMMY_STRING)
                .status(DUMMY_STRING)
                .title(DUMMY_STRING)
                .build();

        taskRequestDto = TaskRequestDto
                .builder()
                .idsOfUsers(List.of(1L))
                .deadline(DUMMY_DATE)
                .description(DUMMY_STRING)
                .status(DUMMY_STRING)
                .title(DUMMY_STRING)
                .build();
    }

    @Test
    @WithMockUser(roles = "ADMIN")
    void givenValidInputWhenCreateThenReturnOk() throws Exception {
        //Arrange
        when(taskService.create(taskRequestDto)).thenReturn(taskResponseDto);
        when(userService.getLoggedUser()).thenReturn(user);

        //Act
        ResultActions resultActions = mockMvc.perform(post(BASE_URL)
                .content(objectMapper.writeValueAsString(taskRequestDto))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON));

        //Assert
        resultActions.andExpect(status().isCreated());
        verify(taskService, times(1)).create(taskRequestDto);
    }
}
