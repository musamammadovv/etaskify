package com.demo.etaskify.controller;

import com.demo.etaskify.dto.OrganisationRequestDto;
import com.demo.etaskify.dto.OrganisationResponseDto;
import com.demo.etaskify.service.OrganisationService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.when;
import static org.mockito.Mockito.verify;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@AutoConfigureMockMvc
@ExtendWith(SpringExtension.class)
@WebMvcTest(OrganisationController.class)
class OrganisationControllerTest {

    private static final Long DUMMY_ID = 1L;
    private static final String DUMMY_STRING = "string";
    private static final String DUMMY_NUMBER = "0552000000";
    private static final String DUMMY_EMAIL = "dummy@dummy.com";
    private static final String BASE_URL = "/api/v1/organisations";

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

    @MockBean
    private OrganisationService organisationService;

    @MockBean
    private UserDetailsService userDetailsService;

    private OrganisationRequestDto organisationRequestDto;
    private OrganisationResponseDto organisationResponseDto;

    @BeforeEach
    void setUp() {
        organisationResponseDto = OrganisationResponseDto
                .builder()
                .id(DUMMY_ID)
                .organisationName(DUMMY_STRING)
                .address(DUMMY_STRING)
                .phoneNumber(DUMMY_NUMBER)
                .build();

        organisationRequestDto = OrganisationRequestDto
                .builder()
                .organisationName(DUMMY_STRING)
                .address(DUMMY_STRING)
                .email(DUMMY_EMAIL)
                .name(DUMMY_STRING)
                .password("123456")
                .phoneNumber(DUMMY_NUMBER)
                .build();
    }

    @Test
    void givenValidInputWhenCreateThenReturnOk() throws Exception {
        //Arrange
        when(organisationService.create(organisationRequestDto)).thenReturn(organisationResponseDto);

        //Act
        ResultActions resultActions = mockMvc.perform(post(BASE_URL)
                .content(objectMapper.writeValueAsString(organisationRequestDto))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON));

        //Assert
        resultActions.andExpect(status().isCreated());
        verify(organisationService, times(1)).create(organisationRequestDto);
    }
}
