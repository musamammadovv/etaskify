package com.demo.etaskify.service.iml;

import com.demo.etaskify.dto.TaskRequestDto;
import com.demo.etaskify.dto.TaskResponseDto;
import com.demo.etaskify.model.Task;
import com.demo.etaskify.model.User;
import com.demo.etaskify.repository.TaskRepository;
import com.demo.etaskify.repository.UserRepository;
import com.demo.etaskify.service.LoggedUserService;
import com.demo.etaskify.service.MailService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;
import org.modelmapper.ModelMapper;
import org.springframework.security.test.context.support.WithMockUser;
import java.time.LocalDate;
import java.util.List;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class TaskServiceImplTest {

    private static final Long DUMMY_ID = 1L;
    private static final String DUMMY_STRING = "string";
    private static final LocalDate DUMMY_DATE = LocalDate.parse("2020-02-28");

    @InjectMocks
    private TaskServiceImpl taskService;

    @Mock
    private UserRepository userRepository;

    @Mock
    private TaskRepository taskRepository;

    @Mock
    private MailService mailService;

    @Mock
    private LoggedUserService loggedUserService;

    @Spy
    private ModelMapper modelMapper;


    private TaskRequestDto taskRequestDto;
    private Task task;
    private User user;

    @BeforeEach
    void setUp() {
        taskRequestDto = getTaskRequestDto();

        task = getTask();

        user = getUser();
    }

    @Test
    @WithMockUser(roles = "ROLE_ADMIN")
    void givenTaskRequestDtoWhenCreateThenTaskResponseDto() {
        //Arrange
        final TaskResponseDto taskResponseDto = modelMapper.map(task, TaskResponseDto.class);
        when(taskRepository.save(any(Task.class))).thenReturn(task);
        when(loggedUserService.getLoggedUser()).thenReturn(user);
        when(userRepository.findById(DUMMY_ID)).thenReturn(java.util.Optional.ofNullable(user));

        //Act
        TaskResponseDto result = taskService.create(taskRequestDto);

        //Assert
        assertThat(result).isEqualTo(taskResponseDto);
    }

    private TaskRequestDto getTaskRequestDto() {
        return TaskRequestDto
                .builder()
                .title(DUMMY_STRING)
                .description(DUMMY_STRING)
                .status(DUMMY_STRING)
                .deadline(DUMMY_DATE)
                .idsOfUsers(List.of(DUMMY_ID))
                .build();
    }

    private Task getTask() {
        return Task
                .builder()
                .title(DUMMY_STRING)
                .description(DUMMY_STRING)
                .status(DUMMY_STRING)
                .deadline(DUMMY_DATE)
                .build();
    }

    private User getUser() {
        return User
                .builder()
                .name(DUMMY_STRING)
                .surname(DUMMY_STRING)
                .email(DUMMY_STRING)
                .build();
    }
}
