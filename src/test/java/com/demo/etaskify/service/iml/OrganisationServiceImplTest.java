package com.demo.etaskify.service.iml;

import com.demo.etaskify.dto.OrganisationRequestDto;
import com.demo.etaskify.dto.OrganisationResponseDto;
import com.demo.etaskify.model.Organisation;
import com.demo.etaskify.model.Role;
import com.demo.etaskify.model.User;
import com.demo.etaskify.repository.OrganisationRepository;
import com.demo.etaskify.repository.RoleRepository;
import com.demo.etaskify.repository.UserRepository;
import com.demo.etaskify.service.LoggedUserService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;
import org.modelmapper.ModelMapper;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class OrganisationServiceImplTest {

    private static final Long DUMMY_ID = 1L;
    private static final String DUMMY_STRING = "string";
    private static final String ROLE_ADMIN = "ROLE_ADMIN";
    private static final String DUMMY_NUMBER = "0505555555";

    @InjectMocks
    private OrganisationServiceImpl organisationService;

    @Mock
    private UserRepository userRepository;

    @Mock
    private RoleRepository roleRepository;

    @Mock
    private OrganisationRepository organisationRepository;

    @Mock
    private LoggedUserService loggedUserService;

    @Spy
    private ModelMapper modelMapper;

    @Spy
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    private OrganisationRequestDto organisationRequestDto;
    private Organisation organisation;
    private Role role;

    @BeforeEach
    void setUp() {
        organisationRequestDto = getOrganisationRequestDto();

        organisation = getOrganisation();

        role = getRole();
    }

    @Test
    void givenOrganisationRequestDtoWhenCreateThenOrganisationResponseDto() {
        //Arrange
        final OrganisationResponseDto organisationResponseDto = modelMapper
                .map(organisation, OrganisationResponseDto.class);
        when(roleRepository.findByName(ROLE_ADMIN)).thenReturn(role);
        when(organisationRepository.save(any(Organisation.class))).thenReturn(organisation);

        //Act
        OrganisationResponseDto result = organisationService.create(organisationRequestDto);

        //Assert
        assertThat(result).isEqualTo(organisationResponseDto);
    }

    private OrganisationRequestDto getOrganisationRequestDto() {
        return OrganisationRequestDto
                .builder()
                .name(DUMMY_STRING)
                .password(bCryptPasswordEncoder.encode(DUMMY_STRING))
                .email(DUMMY_STRING)
                .address(DUMMY_STRING)
                .organisationName(DUMMY_STRING)
                .phoneNumber(DUMMY_NUMBER)
                .address(DUMMY_STRING)
                .build();
    }

    private Organisation getOrganisation() {
        return Organisation
                .builder()
                .address(DUMMY_STRING)
                .organisationName(DUMMY_STRING)
                .phoneNumber(DUMMY_NUMBER)
                .address(DUMMY_STRING)
                .build();
    }

    private Role getRole() {
        return Role
                .builder()
                .id(DUMMY_ID)
                .name(DUMMY_STRING)
                .build();
    }
}
