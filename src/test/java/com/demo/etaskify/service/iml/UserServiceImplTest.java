package com.demo.etaskify.service.iml;

import com.demo.etaskify.dto.UserRequestDto;
import com.demo.etaskify.dto.UserResponseDto;
import com.demo.etaskify.model.Role;
import com.demo.etaskify.model.User;
import com.demo.etaskify.repository.RoleRepository;
import com.demo.etaskify.repository.UserRepository;
import com.demo.etaskify.service.LoggedUserService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;
import org.modelmapper.ModelMapper;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.test.context.support.WithMockUser;
import java.util.List;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class UserServiceImplTest {

    private static final Long DUMMY_ID = 1L;
    private static final String DUMMY_STRING = "string";
    private static final String ROLE_USER = "ROLE_USER";

    @InjectMocks
    private UserServiceImpl userService;

    @Mock
    private UserRepository userRepository;

    @Mock
    private RoleRepository roleRepository;

    @Mock
    private LoggedUserService loggedUserService;

    @Spy
    private ModelMapper modelMapper;

    @Spy
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    private UserRequestDto userRequestDto;
    private UserResponseDto userResponseDto;
    private User user;
    private Role role;

    @BeforeEach
    void setUp() {
        userRequestDto = getUserRequestDto();

        userResponseDto = getUserResponseDto();

        role = getRole();

        user = getUser();
    }

    @Test
    @WithMockUser(roles = "ROLE_ADMIN")
    void givenUserRequestDtoWhenCreateThenUserResponseDto() {
        //Arrange
        final UserResponseDto userResponseDto = modelMapper.map(user, UserResponseDto.class);
        when(roleRepository.findByName(ROLE_USER)).thenReturn(role);
        when(loggedUserService.getLoggedUser()).thenReturn(user);
        when(userRepository.save(any(User.class))).thenReturn(user);

        //Act
        UserResponseDto result = userService.create(userRequestDto);

        //Assert
        assertThat(result).isEqualTo(userResponseDto);
    }

    private UserRequestDto getUserRequestDto() {
        return UserRequestDto
                .builder()
                .name(DUMMY_STRING)
                .surname(DUMMY_STRING)
                .email(DUMMY_STRING)
                .build();
    }

    private UserResponseDto getUserResponseDto() {
        return UserResponseDto
                .builder()
                .name(DUMMY_STRING)
                .surname(DUMMY_STRING)
                .email(DUMMY_STRING)
                .build();
    }

    private User getUser() {
        return User
                .builder()
                .name(DUMMY_STRING)
                .surname(DUMMY_STRING)
                .password(bCryptPasswordEncoder.encode("123456"))
                .email(DUMMY_STRING)
                .roles(List.of(role))
                .build();
    }

    private Role getRole() {
        return role
                .builder()
                .id(DUMMY_ID)
                .name(ROLE_USER)
                .build();
    }
}
