package com.demo.etaskify.configuration;

import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

@Configuration
@RequiredArgsConstructor
public class SecurityConfigAdapter extends WebSecurityConfigurerAdapter {

    private static final String USER_API_V1 = "/api/v1/users**";
    private static final String TASK_API_V1 = "/api/v1/tasks**";
    private static final String ORGANISATION_API_V1 = "/api/v1/organisations**";
    private final UserDetailsService userDetailsService;

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.authorizeRequests()
                .antMatchers(HttpMethod.POST, USER_API_V1).hasAnyRole("ADMIN")
                .antMatchers(HttpMethod.POST, TASK_API_V1).hasAnyRole("ADMIN")
                .antMatchers(HttpMethod.GET, TASK_API_V1).hasAnyRole("ADMIN", "USER")
                .antMatchers(HttpMethod.POST, ORGANISATION_API_V1).permitAll();
        http.csrf().disable().cors();
        super.configure(http);
    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(userDetailsService);
    }

    @Bean
    public BCryptPasswordEncoder bCryptPasswordEncoder() {
        return new BCryptPasswordEncoder();
    }
}
