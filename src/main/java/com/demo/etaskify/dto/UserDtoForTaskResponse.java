package com.demo.etaskify.dto;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class UserDtoForTaskResponse {
    private Long id;
    private String name;
    private String surname;
    private String email;
    private OrganisationDtoForUserResponse organisation;
}
