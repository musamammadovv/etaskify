package com.demo.etaskify.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class OrganisationResponseDto {
    private Long id;
    private String organisationName;
    private String phoneNumber;
    private String address;
    private List<UserDtoForOrganisationResponse> users;
}
