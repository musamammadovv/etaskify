package com.demo.etaskify.dto;

import lombok.Data;

@Data
public class OrganisationDtoForUserResponse {
    private Long id;
    private String organisationName;
    private String phoneNumber;
    private String address;
}
