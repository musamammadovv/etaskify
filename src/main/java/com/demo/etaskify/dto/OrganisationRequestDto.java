package com.demo.etaskify.dto;

import lombok.Builder;
import lombok.Data;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

@Data
@Builder
public class OrganisationRequestDto {

    @NotBlank
    private String organisationName;

    @NotBlank
    @Size(min = 7, max = 12)
    @Pattern(regexp = "^[0-9]+$")
    private String phoneNumber;

    @NotBlank
    private String address;

    @NotBlank
    private String name;

    @Email
    @NotBlank
    private String email;

    @NotBlank
    @Size(min = 6, max = 18)
    @Pattern(regexp = "^[A-Za-z0-9]+$")
    private String password;
}
