package com.demo.etaskify.dto;

import lombok.Data;
import java.time.LocalDate;

@Data
public class TaskDtoForUserResponse {
    private Long id;
    private String title;
    private String description;
    private LocalDate deadline;
    private String status;
}
