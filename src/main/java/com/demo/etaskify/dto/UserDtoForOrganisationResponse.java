package com.demo.etaskify.dto;

import lombok.Data;

import java.util.List;

@Data
public class UserDtoForOrganisationResponse {
    private Long id;
    private String name;
    private String surname;
    private String email;
    private List<TaskResponseDto> tasks;
}
