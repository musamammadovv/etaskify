package com.demo.etaskify.controller;

import com.demo.etaskify.dto.TaskRequestDto;
import com.demo.etaskify.dto.TaskResponseDto;
import com.demo.etaskify.service.LoggedUserService;
import com.demo.etaskify.service.TaskService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import javax.validation.Valid;

@Slf4j
@Controller
@RequiredArgsConstructor
@RequestMapping("/api/v1/tasks")
public class TaskController {

    private final TaskService taskService;
    private final LoggedUserService userService;

    @PostMapping
    public ResponseEntity<TaskResponseDto> create(@Valid @RequestBody TaskRequestDto taskRequestDto) {
        log.trace("Request to create task by {}", userService.getLoggedUser().getEmail());
        return ResponseEntity.status(HttpStatus.CREATED).body(taskService.create(taskRequestDto));
    }

    @GetMapping
    public ResponseEntity<Page<TaskResponseDto>> get(Pageable pageable) {
        log.trace("Request to get tasks by {}", userService.getLoggedUser().getEmail());
        return ResponseEntity.status(HttpStatus.OK).body(taskService.get(pageable));
    }
}
