package com.demo.etaskify.controller;

import com.demo.etaskify.dto.OrganisationRequestDto;
import com.demo.etaskify.dto.OrganisationResponseDto;
import com.demo.etaskify.service.OrganisationService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import javax.validation.Valid;

@Slf4j
@Controller
@RequiredArgsConstructor
@RequestMapping("/api/v1/organisations")
public class OrganisationController {

    private final OrganisationService organisationService;

    @PostMapping
    public ResponseEntity<OrganisationResponseDto> create(
            @Valid @RequestBody OrganisationRequestDto organisationRequestDto) {
        log.trace("Request to create organisation by {}", organisationRequestDto.getEmail());
        return ResponseEntity.status(HttpStatus.CREATED).body(organisationService.create(organisationRequestDto));
    }
}
