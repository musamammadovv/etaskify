package com.demo.etaskify.controller;

import com.demo.etaskify.dto.UserRequestDto;
import com.demo.etaskify.dto.UserResponseDto;
import com.demo.etaskify.service.LoggedUserService;
import com.demo.etaskify.service.UserService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.validation.Valid;

@Slf4j
@Controller
@RequiredArgsConstructor
@RequestMapping("/api/v1/users")
public class UserController {

    private final LoggedUserService loggedUserService;
    private final UserService userService;

    @PostMapping
    public ResponseEntity<UserResponseDto> create(@Valid @RequestBody UserRequestDto userRequestDto) {
        log.trace("Request to create user by {}", loggedUserService.getLoggedUser().getEmail());
        return ResponseEntity.status(HttpStatus.CREATED).body(userService.create(userRequestDto));
    }
}
