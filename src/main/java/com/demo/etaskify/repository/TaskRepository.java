package com.demo.etaskify.repository;

import com.demo.etaskify.model.Task;
import com.demo.etaskify.model.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TaskRepository extends JpaRepository<Task, Long> {
    Page<Task> findByUsers(User user, Pageable pageable);
}
