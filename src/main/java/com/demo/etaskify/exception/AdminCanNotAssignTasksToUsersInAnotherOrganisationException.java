package com.demo.etaskify.exception;

public class AdminCanNotAssignTasksToUsersInAnotherOrganisationException extends RuntimeException {

    private static final long serialVersionUID = 58432532487812L;

    private static final String MESSAGE = "Admin can not assign tasks to users in another organisation!";

    public AdminCanNotAssignTasksToUsersInAnotherOrganisationException() {
        super(MESSAGE);
    }
}
