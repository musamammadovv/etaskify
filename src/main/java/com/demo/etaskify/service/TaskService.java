package com.demo.etaskify.service;

import com.demo.etaskify.dto.TaskRequestDto;
import com.demo.etaskify.dto.TaskResponseDto;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface TaskService {
    TaskResponseDto create(TaskRequestDto taskRequestDto);

    Page<TaskResponseDto> get(Pageable pageable);
}
