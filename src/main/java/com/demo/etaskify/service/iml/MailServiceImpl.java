package com.demo.etaskify.service.iml;

import com.demo.etaskify.dto.TaskRequestDto;
import com.demo.etaskify.repository.UserRepository;
import com.demo.etaskify.service.MailService;
import lombok.RequiredArgsConstructor;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class MailServiceImpl implements MailService {

    private final JavaMailSender javaMailSender;
    private final UserRepository userRepository;

    @Override
    public void sendEmailToUsersAboutTask(TaskRequestDto task) {
        task.getIdsOfUsers().forEach(id -> {
            String email = userRepository.findById(id).get().getEmail();
            javaMailSender.send(getMessageWithDetails(task, email));
        });
    }

    private SimpleMailMessage getMessageWithDetails(TaskRequestDto task, String email) {
        SimpleMailMessage msg = new SimpleMailMessage();
        msg.setTo(email);
        msg.setSubject("The task with title \"" + task.getTitle() + "\" has been assigned to you");
        msg.setText("Task description: " + task.getDescription());
        return msg;
    }
}
