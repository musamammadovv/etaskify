package com.demo.etaskify.service.iml;

import com.demo.etaskify.dto.TaskRequestDto;
import com.demo.etaskify.dto.TaskResponseDto;
import com.demo.etaskify.exception.AdminCanNotAssignTasksToUsersInAnotherOrganisationException;
import com.demo.etaskify.exception.UserNotFoundException;
import com.demo.etaskify.model.Task;
import com.demo.etaskify.model.User;
import com.demo.etaskify.repository.TaskRepository;
import com.demo.etaskify.repository.UserRepository;
import com.demo.etaskify.service.LoggedUserService;
import com.demo.etaskify.service.MailService;
import com.demo.etaskify.service.TaskService;
import com.demo.etaskify.service.UserService;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import java.util.ArrayList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class TaskServiceImpl implements TaskService {

    private final ModelMapper modelMapper;
    private final UserRepository userRepository;
    private final TaskRepository taskRepository;
    private final UserService userService;
    private final MailService mailService;
    private final LoggedUserService loggedUserService;

    @Override
    @Transactional
    public TaskResponseDto create(TaskRequestDto taskRequestDto) {
        Task task = assignTaskToUsersAndSave(modelMapper.map(taskRequestDto, Task.class), taskRequestDto);
        mailService.sendEmailToUsersAboutTask(taskRequestDto);
        return modelMapper.map(task, TaskResponseDto.class);
    }

    @Override
    @Transactional
    public Page<TaskResponseDto> get(Pageable pageable) {
        User user = loggedUserService.getLoggedUser();
        return taskRepository.findByUsers(user, pageable).map(task -> modelMapper.map(task, TaskResponseDto.class));
    }

    private Task assignTaskToUsersAndSave(Task task, TaskRequestDto taskRequestDto) {
        task.setUsers(getUsersByIds(taskRequestDto));
        return taskRepository.save(task);
    }

    private List<User> getUsersByIds(TaskRequestDto taskRequestDto) {
        List<User> users = new ArrayList<>();
        taskRequestDto.getIdsOfUsers().forEach(id -> {
            User user = userRepository.findById(id).orElseThrow(UserNotFoundException::new);
            checkIfAdminAttendsToAddTaskToUserInAnotherOrganisation(user);
            users.add(user);
        });
        return users;
    }

    private void checkIfAdminAttendsToAddTaskToUserInAnotherOrganisation(User user) {
        User loggedUser = loggedUserService.getLoggedUser();
        if (user.getOrganisation() != loggedUser.getOrganisation()) {
            throw new AdminCanNotAssignTasksToUsersInAnotherOrganisationException();
        }
    }
}
