package com.demo.etaskify.service.iml;

import com.demo.etaskify.dto.UserRequestDto;
import com.demo.etaskify.dto.UserResponseDto;
import com.demo.etaskify.model.User;
import com.demo.etaskify.repository.RoleRepository;
import com.demo.etaskify.repository.UserRepository;
import com.demo.etaskify.service.LoggedUserService;
import com.demo.etaskify.service.UserService;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;
    private final RoleRepository roleRepository;
    private final ModelMapper modelMapper;
    private final BCryptPasswordEncoder bCryptPasswordEncoder;
    private final LoggedUserService loggedUserService;

    @Override
    public UserResponseDto create(UserRequestDto userRequestDto) {
        User user = modelMapper.map(userRequestDto, User.class);
        user.setOrganisation(loggedUserService.getLoggedUser().getOrganisation());
        user.setPassword(bCryptPasswordEncoder.encode("123456"));
        user.setRoles(List.of(roleRepository.findByName("ROLE_USER")));
        return modelMapper.map(userRepository.save(user), UserResponseDto.class);
    }
}
