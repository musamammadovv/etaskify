package com.demo.etaskify.service.iml;

import com.demo.etaskify.dto.OrganisationRequestDto;
import com.demo.etaskify.dto.OrganisationResponseDto;
import com.demo.etaskify.model.Organisation;
import com.demo.etaskify.model.User;
import com.demo.etaskify.repository.OrganisationRepository;
import com.demo.etaskify.repository.RoleRepository;
import com.demo.etaskify.repository.UserRepository;
import com.demo.etaskify.service.OrganisationService;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import java.util.List;

@Service
@RequiredArgsConstructor
public class OrganisationServiceImpl implements OrganisationService {

    private final ModelMapper modelMapper;
    private final UserRepository userRepository;
    private final OrganisationRepository organisationRepository;
    private final RoleRepository roleRepository;
    private final BCryptPasswordEncoder bCryptPasswordEncoder;

    @Override
    public OrganisationResponseDto create(OrganisationRequestDto organisationRequestDto) {
        return modelMapper.map(saveOrganisation(organisationRequestDto), OrganisationResponseDto.class);
    }

    private User getAdmin(OrganisationRequestDto organisationRequestDto, Organisation organisation) {
        User user = modelMapper.map(organisationRequestDto, User.class);
        user.setOrganisation(organisation);
        user.setRoles(List.of(roleRepository.findByName("ROLE_ADMIN")));
        user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
        return user;
    }

    private Organisation saveOrganisation(OrganisationRequestDto organisationRequestDto) {
        Organisation organisation = modelMapper.map(organisationRequestDto, Organisation.class);
        organisation.setUsers(List.of(getAdmin(organisationRequestDto, organisation)));
        return organisationRepository.save(organisation);
    }
}
