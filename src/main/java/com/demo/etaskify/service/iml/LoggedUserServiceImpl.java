package com.demo.etaskify.service.iml;

import com.demo.etaskify.model.User;
import com.demo.etaskify.repository.UserRepository;
import com.demo.etaskify.service.LoggedUserService;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class LoggedUserServiceImpl implements LoggedUserService {

    private final UserRepository userRepository;

    @Override
    public User getLoggedUser() {
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        String email;
        if (principal instanceof UserDetails) {
            email = ((UserDetails) principal).getUsername();
        } else {
            email = principal.toString();
        }
        return userRepository.findByEmail(email).get();
    }
}
