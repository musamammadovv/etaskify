package com.demo.etaskify.service;

import com.demo.etaskify.dto.OrganisationRequestDto;
import com.demo.etaskify.dto.OrganisationResponseDto;

public interface OrganisationService {
    OrganisationResponseDto create(OrganisationRequestDto organisationRequestDto);
}
