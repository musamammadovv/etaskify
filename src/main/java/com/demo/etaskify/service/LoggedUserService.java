package com.demo.etaskify.service;

import com.demo.etaskify.model.User;

public interface LoggedUserService {
    User getLoggedUser();
}
