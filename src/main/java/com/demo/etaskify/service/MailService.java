package com.demo.etaskify.service;

import com.demo.etaskify.dto.TaskRequestDto;

public interface MailService {
    void sendEmailToUsersAboutTask(TaskRequestDto task);
}
