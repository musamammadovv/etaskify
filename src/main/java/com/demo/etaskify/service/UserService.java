package com.demo.etaskify.service;

import com.demo.etaskify.dto.UserRequestDto;
import com.demo.etaskify.dto.UserResponseDto;

public interface UserService {
    UserResponseDto create(UserRequestDto userRequestDto);
}
